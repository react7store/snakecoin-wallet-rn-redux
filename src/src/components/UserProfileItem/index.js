/** @format */

import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  I18nManager,
} from 'react-native'
import { Images, Languages, Constants } from '@common'
import { Back } from '@navigation/IconNav'
import Icon from 'react-native-vector-icons/Entypo'
import _ from 'lodash'

import styles from './styles'

const { width, height } = Dimensions.get('window')

export default class UserProfileItem extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
  }

  static defaultProps = {
    icon: false,
  }

  render() {
    const { label, value, onPress, icon } = this.props

    return (
      <View style={styles.row}>
        <Text style={styles.leftText}>{label}</Text>
        <TouchableOpacity onPress={onPress} style={styles.rightContainer}>
          <Text style={styles.rightText}>{value}</Text>
          {icon &&
            _.isBoolean(icon) && (
              <Icon
                style={[
                  styles.icon,
                  I18nManager.isRTL && {
                    transform: [{ rotate: '180deg' }],
                  },
                ]}
                color="#CCCCCC"
                size={22}
                name="chevron-small-right"
              />
            )}
          {icon && !_.isBoolean(icon) && icon()}
        </TouchableOpacity>
      </View>
    )
  }
}
