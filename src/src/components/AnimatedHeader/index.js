/** @format */

import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Animated, Platform, Text, View, I18nManager } from 'react-native'
import { Menu , Back} from '@navigation/IconNav'
import { Languages } from '@common'
import styles from './styles'

export default class AnimatedHeader extends PureComponent {
  static propTypes = {
    scrollY: PropTypes.any,
    label: PropTypes.string,
    hideIcon: PropTypes.bool,
  }

  render() {
    const { navigation, scrollY, label, backIcon,  hideIcon } = this.props
    const title = Languages[label] || label

    if (Platform.OS === 'android' || typeof scrollY == 'undefined') {
      return (
        <View style={styles.headerAndroid}>
          {label && <Text style={styles.headerLabelStatic}>{label}</Text>}
          {!hideIcon && <View style={styles.homeMenu}>{Menu()}</View>}
          <View style={styles.homeMenu}>{!backIcon ? Menu() : Back(navigation)}</View>
        </View>
      )
    }

    const titleTransformY = scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [0, -45],
      extrapolate: 'clamp',
    })
    const titleTransformX = scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [0, I18nManager.isRTL ? -25 : 25],
      extrapolate: 'clamp',
    })
    const titleScale = scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [1, 0.8],
      extrapolate: 'clamp',
    })

    return (
      <View>
        <View style={styles.headerView} />

        <Animated.Text
          style={[
            styles.headerLabel,
            {
              transform: [
                { translateY: titleTransformY },
                { translateX: titleTransformX },
                { scale: titleScale },
              ],
            },
          ]}>
          {title}
        </Animated.Text>
        {!hideIcon && <View style={styles.homeMenu}>{Menu()}</View>}
        {backIcon && <View style={styles.homeMenu}>{Back(navigation)}</View>}
      </View>
    )
  }
}
