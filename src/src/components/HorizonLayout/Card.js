'use strict';
import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import css from "./style";
import {Constants, Languages, Images, Styles, Tools} from "@common";
import {ProductPrice, Rating, ImageCache, WishListIcon} from "@components";
import {getProductImage} from "@app/Omni"

export default class CardLayout extends Component {

  render() {
    const {product, title, viewPost, viewVendor, store} = this.props;
    const imageURI = typeof product.images[0] != 'undefined' ? getProductImage(product.images[0].src, Styles.width) : Images.PlaceHolderURL

    return (
      <TouchableOpacity activeOpacity={0.9} style={css.panelCard} onPress={viewPost}>
        <ImageCache uri={imageURI} style={css.imagePanelCard}/>
        <Text numberOfLines={2} style={css.nameCard}>{title}</Text>
        <TouchableOpacity onPress={viewVendor}>
          <Text style={css.vendorName}>{store.toUpperCase()}</Text>
        </TouchableOpacity>
        <ProductPrice product={product} hideDisCount/>
        <WishListIcon product={product}/>
        <Rating rating={Number(product.average_rating)}/>
      </TouchableOpacity>
    );
  }
}
