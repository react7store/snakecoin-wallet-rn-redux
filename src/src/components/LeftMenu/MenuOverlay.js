import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Drawer, SideMenu } from '@components'

class MenuOverlay extends PureComponent {
  static propTypes = {
    goToScreen: PropTypes.func,
    toggleMenu: PropTypes.func,
    routes: PropTypes.object,
    isOpenMenu: PropTypes.bool.isRequired,
  }

  toggleMenu = (isOpen) => {
    if (!isOpen) {
      this.props.toggleMenu(isOpen)
    }
  }

  render() {
    const { isOpenMenu } = this.props
    const drawerStyles = {
      drawer: { backgroundColor: '#FFF' },
      main: { paddingLeft: 0, paddingRight: 0 },
    }
    return (
      <SideMenu
        ref={(_drawer) => (this.drawer = _drawer)}
        type="static"
        styles={drawerStyles}
        isOpen={isOpenMenu}
        onChange={(isOpen) => this.toggleMenu(isOpen)}
        useInteractionManager
        menu={<Drawer goToScreen={this.props.goToScreen} />}>
        {this.props.routes}
      </SideMenu>
    )
  }
}

const mapStateToProps = ({ sideMenu }) => ({
  isOpenMenu: sideMenu.isOpen,
})
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dispatch } = dispatchProps
  const { actions: sideMenuActions } = require('@redux/SideMenuRedux')
  return {
    ...ownProps,
    ...stateProps,
    toggleMenu: (isOpen) => dispatch(sideMenuActions.toggleMenu(isOpen)),
  }
}
export default connect(mapStateToProps, null, mergeProps)(MenuOverlay)
