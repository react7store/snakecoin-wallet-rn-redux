import React, { PureComponent } from "react";
import { View, Text, Image, Dimensions } from "react-native";
import HTML from 'react-native-render-html'
import { Tools, Constants } from "@common";
import styles from './styles'

export default class Index extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fontSize: Constants.fontText.size
    };

    Tools.getFontSizePostDetail().then((data) => {
      this.setState({ fontSize: data })
    })
  }

  render() {
    const htmlContent = this.props.html;
    const tagsStyles = {
      p: { margin: 0, padding: 0 , lineHeight: 22, textAlign: 'justify' },
      li: { color: "#666", lineHeight: 22 }
    }

    return <HTML html={htmlContent} containerStyle={styles.container}
        tagsStyles={tagsStyles}
        renderers={
          {
            img: (htmlAttribs, children, convertedCSSStyles, passProps) => {
              const { src, alt, width, height } = htmlAttribs;
              if (!src) {
                return false;
              }
              const newWidth = Dimensions.get('window').width - 20;
              const newHeight = height * newWidth / width;
              return <Image source={{ uri: src }}
                style={{ width: newWidth, height: newHeight, resizeMode: 'contain', marginVertical: 25 }} />
            }
          }
        }
      />

  }
}
