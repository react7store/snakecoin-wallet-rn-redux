/** @format */

import React, { Component } from 'react'
import {
  FlatList,
  Image,
  Platform,
  RefreshControl,
  Animated,
  View,
} from 'react-native'
import {
  PostLayout,
  AnimatedHeader,
  Spinkit,
} from '@components'
import { Constants, Images } from '@common'
import { connect } from 'react-redux'
import styles from './styles'
import {warn} from '@app/Omni'

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)

class ProductListVendor extends Component {
  state = { scrollY: new Animated.Value(0) }

  constructor(props) {
    super(props)
    this.page = props.page ? props.page : 0
    this.limit = Constants.pagingLimit
    this.isProductList = props.type === undefined
  }

  componentDidMount() {
    this.page === 0 && this.fetchData()
  }

  fetchData = (reload = false) => {
    if (reload) {
      this.page = 1
    }
    const { vendor, fetchProducts, fetchVendor } = this.props
    fetchVendor(vendor.id)
    fetchProducts(vendor.id, 20, this.page)
  }

  fetchMore = () => {
    this.page += 1
    this.fetchData()
  }

  onRowClickHandle = (item) => {
    if (this.isProductList) {
      this.props.onViewProductScreen({ product: item })
    } else {
      this.props.onViewNewsScreen({ post: item })
    }
  }

  onViewVendor = (product) => {
    this.props.onViewVendor(product.store)
  }

  renderItem = ({ item, index }) => {
    if (item == null) return <View />

    const layout = Constants.Layout.twoColumn

    return (
      <PostLayout
        post={item}
        type={this.props.type}
        key={`key-${index}`}
        onViewPost={this.onRowClickHandle.bind(this, item, this.props.type)}
        onViewVendor={this.onViewVendor.bind(this, item, this.props.type)}
        layout={layout}
      />
    )
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.list !== this.props.list;
  }

  headerComponent = () => {
    const { vendorInfo } = this.props
    const image = (vendorInfo != null && vendorInfo.banner) ? {uri:vendorInfo.banner} : Images.categoryPlaceholder
    // console.log(image)
    return (
      <View style={styles.headerView}>
          <Image style={styles.bannerImage}
              source={image} />
      </View>
    )
  }

  render() {
    const { list, vendor , isFetching } = this.props
    const shopName = (typeof vendor != 'undefined' && vendor != null) ?  vendor.shop_name || vendor.store_name : ''
    const renderFooter = () => isFetching && <Spinkit />;

    return (
      <View style={styles.listView}>
        <AnimatedHeader
          scrollY={this.state.scrollY}
          hideIcon
          backIcon
          navigation={this.props.navigation}
          label={shopName}
        />
        <AnimatedFlatList
          contentContainerStyle={styles.flatlist}
          data={list}
          numColumns={2}
          keyExtractor={(item, index) => `${item.id} || ${index}`}
          renderItem={this.renderItem}
          ListHeaderComponent={this.headerComponent}
          ListFooterComponent={renderFooter()}
          refreshing={isFetching}
          refreshControl={
            <RefreshControl
              refreshing={isFetching}
              onRefresh={() => this.fetchData(true)}
            />
          }
          onEndReachedThreshold={100}
          onEndReached={(distance) =>
            distance.distanceFromEnd > 100 && this.fetchMore()
          }
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: Platform.OS != 'android' }
          )}
        />
      </View>
    )
  }
}

const mapStateToProps = ({ vendor }, ownProps) => {
  const list = vendor.products
  const isFetching = vendor.isFetching
  return { list, isFetching,
    vendor: ownProps.vendor,
    vendorInfo: vendor.vendor
  }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dispatch } = dispatchProps
  const Vendor = require('@redux/VendorRedux')
  return {
    ...ownProps,
    ...stateProps,
    fetchProducts: (vendorId, per_page, page) => {
      return Vendor.actions.fetchProductsByVendor(dispatch,vendorId,per_page,page)
    },
    fetchVendor: (vendorId) => {
      return Vendor.actions.fetchVendor(dispatch,vendorId)
    },
  }
}

export default connect(mapStateToProps, null, mergeProps)(ProductListVendor)