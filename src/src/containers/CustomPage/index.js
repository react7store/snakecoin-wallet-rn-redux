'use strict';
import React, {Component} from "react";
import {View} from 'react-native'
import wp from "@services/WPAPI";
import WebView from "@components/WebView/WebView";

export default class CustomPage extends Component {
  constructor(props) {
    super(props);
    this.state = {html: ''};
  }

  componentWillMount() {
    this.fetchPage(this.props.id);
  }

  componentWillReceiveProps(nextProps) {
    this.fetchPage(nextProps.id)
  }

  fetchPage(id) {
    wp.pages().id(id)
      .get((err, data) => {
        if (data) {
          this.setState({
            html: typeof data.content.rendered != 'undefined' ? data.content.rendered : "Content is updating"
          })
        }
      });
  }

  render() {
    return (<View style={{ flex: 1, backgroundColor: '#FFF' }}>
        <WebView html={this.state.html}/>
      </View>
    )
  }
}
