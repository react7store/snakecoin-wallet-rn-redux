/** @format */

import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View, FlatList, Text, TouchableOpacity, TextInput } from 'react-native'
import {Constants, Languages} from '@common'
import { toast } from '@app/Omni'
import {SearchBar, PostLayout} from '@components'
import IconIO from '@expo/vector-icons/Ionicons'

import { connect } from 'react-redux'
import DefaultSearch from './DefaultSearch'
import styles from './styles'

class SearchScreen extends PureComponent {
  state = {textSearch: ''};

  static defaultProps = {
    listSearch: [],
  }

  componentDidMount() {
    const {
      fetchCategories,
      fetchAllProducts,
      fetchAllVendors,
      listCategories,
    } = this.props
    fetchAllProducts(10, 1);
    fetchAllVendors();
    if (listCategories && listCategories.length === 0) {
      fetchCategories()
    }
  }

  _onSearch = (text) => {
    this.setState({textSearch: text})
    this.props.fetchProductsByName(text)
  }

  _stopSearch = () => {
    this.props.stopSearch()
  }

  _showCategory = (category) => {
    const { setSelectedCategory, onViewCategory } = this.props
    setSelectedCategory(category)
    onViewCategory(category)
  }

  _onViewProductScreen= (item, index, isCategory) => {
    if (isCategory) {
      this._showCategory(item)
    } else {
      this.props.onViewProductScreen(item)
    }
  }

  _onViewVendor= (item) => {
      this.props.onViewVendor(item)
  }

  // _closeSearch = () => {
  //   const {clearSearchPosts, stopSearchPosts} = this.props
  //   clearSearchPosts()
  //   stopSearchPosts()
  // }


  _renderHeader = () => {
    const {selected} = this.props
    const hitSlop = { top: 25, right: 25, left: 25, bottom: 25 }
    if(typeof selected != 'undefined' && selected.length > 0 ){
      return <View style={styles.filters}>
          <Text style={styles.txtFilter}>{Languages.filter}</Text>
          {selected.map((item,index) => {
            return <Text style={styles.txtText}>{item.name + ', '}</Text>
          })}
          <TouchableOpacity style={styles.closeWrap} hitSlop={hitSlop} onPress={this._closeSearch}>
            <IconIO style={styles.btnCloseSearch} hitSlop={hitSlop}
              name={"ios-close-circle-outline"}
              size={20} color={'#000'} backgroundColor="transparent"
            />
          </TouchableOpacity>
      </View>
    }
    return <View />
  }

  _keyExtractor = (item, index) => index.toString()

  _renderNoResult = () => {
    return <View style={styles.msgWrap}>
        <Text style={styles.msg}>{Languages.NoResultError}</Text>
      </View>
  }

  _renderContent = () => {
    const {listProducts, listVendors, searched, isFetchingSearch, listSearch, listCategories } = this.props

    if (searched && listSearch && listSearch.length > 0 && this.state.textSearch.length > 0) {
      return (
        <FlatList
          data={listSearch}
          keyExtractor={this._keyExtractor}
          // ListHeaderComponent={this._renderHeader}
          renderItem={({ item, index }) => (
            <PostLayout
              key={index}
              post={item}
              onViewPost={() => this._onViewProductScreen(item, index)}
              layout={Constants.Layout.simple}
            />
          )}
        />
      )
    } else if(searched && !isFetchingSearch && listSearch.length == 0 && this.state.textSearch.length > 0){
        return this._renderNoResult()
    }
    return <DefaultSearch {...this.props} 
                          onViewProductScreen={this._onViewProductScreen} 
                          onViewVendor={this._onViewVendor} />  
    
  }

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          style={styles.searchBar}
          stopSearch={this._stopSearch}
          onSearch={this._onSearch}
        />
        {this._renderContent()}
      </View>
    )
  }
}




const mapStateToProps = ({products, categories, vendor}) => {
  return {
    listProducts: products.listAll,
    listCategories: categories.list,
    listVendors: vendor.list,
    listSearch: products.productsByName,
    isFetchingSearch: products.isFetchingSearch,
    searched: products.isSearched,
  }
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps
  const CategoryRedux = require('@redux/CategoryRedux')
  const ProductRedux = require('@redux/ProductRedux')
  const VendorRedux = require('@redux/VendorRedux')

  return {
    ...ownProps,
    ...stateProps,
    fetchCategories: () => CategoryRedux.actions.fetchCategories(dispatch),
    fetchAllProducts: (per_page, page) => ProductRedux.actions.fetchAllProducts(dispatch, per_page, page),
    fetchAllVendors: () => VendorRedux.actions.fetchAllVendors(dispatch),
    setSelectedCategory: (category) => dispatch(CategoryRedux.actions.setSelectedCategory(category)),
    fetchProductsByName: (name, per_page, page) => {
      if (name.length > 2) {
        ProductRedux.actions.fetchProductsByName(dispatch, name, per_page, page)
      }
    },
    stopSearch: () => ProductRedux.actions.stopSearch(dispatch)
  }
}

export default connect(mapStateToProps, undefined, mergeProps)(SearchScreen)