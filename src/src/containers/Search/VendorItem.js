/** @format */

'use strict'
import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import styles from './styles'
import {Images, Tools, Config } from '@common'
import { LinearGradient } from 'expo'

export default class VendorItem extends Component {
  render() {
    const { onViewVendor, data, width, height, styleImage } = this.props

    const image = (data.social.flickr != null &&  data.social.flickr) ? {uri : data.social.flickr } : Images.vendorPlaceholder

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={styles.panelCircle}
        onPress={onViewVendor}>
        <View style={styles.wrapImg}>
            <Image
            source={image}
            style={[
                styles.imagePanelCircle,
                width && { width },
                height && { height },
                styleImage,
            ]}
            />
        </View>
        <View style={styles.titleCircle}>
          <Text style={styles.titleCir}>
            {Tools.getDescription(data.store_name, 200)}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}
