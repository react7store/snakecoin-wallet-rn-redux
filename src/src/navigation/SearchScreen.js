/** @format */

import React, { PureComponent } from 'react'
import { Search } from '@containers'
import { Config, Styles, Constants, Images } from '@common'
import { warn } from '@app/Omni'

export default class SearchScreen extends PureComponent {
  static navigationOptions = () => ({
    title: 'Search',
    header: null,
    // tabBarVisible: false,

    tabBarLabel: null,
  })

  render() {
    const { navigate, goBack } = this.props.navigation

    return (
      <Search
        onBack={goBack}
        onViewCategory={(item) => {
          navigate('CategoryScreen', item)
        }}
        onViewVendor={(store) => navigate('Vendor', {store})}
        onViewProductScreen={(product) => navigate('DetailScreen', {product})}
        navigation={this.props.navigation}
      />
    )
  }
}
