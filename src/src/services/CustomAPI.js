/** @format */
import { Config } from '@common'
import {Platform} from 'react-native'

/**
 * init class API
 * @param opt
 * @returns {WordpressAPI}
 * @constructor
 */
function WordpressAPI(opt) {
  if (!(this instanceof WordpressAPI)) {
    return new WordpressAPI(opt)
  }
  opt = opt || {}
  this._setDefaultsOptions(opt)
}


/**
 * Default option
 * @param opt
 * @private
 */
WordpressAPI.prototype._setDefaultsOptions = async function(opt) {
  this.url = opt.url ? opt.url : Config.WooCommerce.url
  this.namespace = opt.namespace ? opt.namespace  : 'mstore/v1'
  this.namespaceDokan = opt.namespaceDokan ? opt.namespaceDokan  : 'dokan/v1'
}


WordpressAPI.prototype.getProductsByVendor = async function(data) {
  let storeId = data.storeId

  let requestUrl = this.url + '/wp-json/' + this.namespace + '/stores/'+ storeId + '/products';

  return this._request(requestUrl).then(function(data) {
    return data
  })
}

WordpressAPI.prototype.getVendor = async function(vendorId) {

  let requestUrl = this.url + '/wp-json/' + this.namespaceDokan + '/stores/'+ vendorId;

  return this._request(requestUrl).then(function(data) {
    return data
  })
}

WordpressAPI.prototype.getAllVendors = async function() {

  let requestUrl = this.url + '/wp-json/' + this.namespaceDokan + '/stores/';

  return this._request(requestUrl).then(function(data) {
    return data
  })
}


WordpressAPI.prototype.createComment = async function(data, callback) {
  let requestUrl = this.url + '/api/mstore_user/post_comment/?insecure=cool'
  if (data) {
    requestUrl += '&' + this.join(data, '&')
  }
  // console.log(requestUrl)
  return this._request(requestUrl, data, callback)
}


WordpressAPI.prototype.join = function(obj, separator) {
  var arr = []
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      arr.push(key + '=' + obj[key])
    }
  }
  return arr.join(separator)
}
WordpressAPI.prototype._request = function(url, callback) {
  var self = this
  return fetch(url)
    .then(response => response.text()) // Convert to text instead of res.json()
    .then(text => {
      if (Platform.OS === 'android') {
        text = text.replace(/\r?\n/g, '').replace(/[\u0080-\uFFFF]/g, '') // If android , I've removed unwanted chars.
      }
      return text
    })
    .then(response => JSON.parse(response))

    .catch((error, data) => {
      // console.log('1=error network -', error, data);
    })
    .then(responseData => {
      if (typeof callback == 'function') {
        callback()
      }
      // console.log('request result from ' + url, responseData);

      return responseData
    })
    .catch(error => {
      // console.log('2=error network -- ', error.message);
    })
}
/**
 * Post to the server
 * @param url
 * @param data
 * @param callback
 * @returns {axios.Promise}
 * @private
 */
WordpressAPI.prototype._requestPost = function(url, data, callback) {
  var self = this

  var params = {
    method: 'POST',
    // headers: {
    //   'Accept':       'application/json',
    //   'Content-Type': 'application/json',
    //   // 'X-CSRFToken':  cookie.load('csrftoken')
    //
    // },
    // credentials: 'same-origin',
    // mode: 'same-origin',
    body: JSON.stringify(data),
  }
  return fetch(url, params)
    .then(response => response.json())

    .catch((error, data) => {
      // console.log('error network', error);
    })
    .then(responseData => {
      if (typeof callback == 'function') {
        callback()
      }
      return responseData
    })
    .catch(error => {
      // console.log('error network', error.message);
    })
}


export default new WordpressAPI
