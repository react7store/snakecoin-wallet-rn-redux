/** @format */

export default {
  Exit: "Exit",
  ExitConfirm: "Are you sure you want to exit snakecoinWallet",
  YES: "YES",
  OK: "OK",
  ViewMyOrders: "View My Topup",
  CANCEL: "CANCEL",
  Confirm: "Confirm",

  // Scene's Titles
  Home: "Home",
  Intro: "Intro",
  Product: "Product",
  Cart: "Cart",
  WishList: "WishList",

  // Home
  products: "products",

  // TopBarz
  ShowFilter: "Sub Categories",
  HideFilter: "Hide",
  Sort: "Sort",
  textFilter: "Recent",

  // Category
  ThereIsNoMore: "There is no more product to show",

  // Product
  AddtoCart: "Add to Cart",
  AddtoWishlist: "Add to Wishlist",
  ProductVariations: "Variations",
  NoVariation: "This product don't have any variation",
  AdditionalInformation: "Description",
  NoProductDescription: "No Product Description",
  ProductReviews: "Reviews",
  NoReview: "This product don't have any reviews ...yet",
  BUYNOW: "TOP UP NOW",
  OutOfStock: "OFFLINE",
  ProductLimitWaring: "You can't add more than 10 product",
  EmptyProductAttribute: "This transaction don't have any attributes",
  ProductFeatures: "Features",
  ErrorMessageRequest: "Can't get data from server",
  NoConnection: "No internet connection",
  ProductRelated: "Related Products",

  // Cart
  NoCartItem: "There is no product in cart",
  Total: "Total",
  EmptyCheckout: "Sorry, you can't check out an empty cart",
  RemoveCartItemConfirm: "Remove this recharge plan from cart?",
  MyCart: "Cart",
  Order: "Order",
  ShoppingCart: "Shopping Cart",
  ShoppingCartIsEmpty: "Your Cart is Empty",
  AddProductToCart: "Add a recharge plan to the shopping cart",
  TotalPrice: "Total Price:",
  YourDeliveryInfo: "Your Shipping Detail",
  ShopNow: "Choose Your EWallet Plan Now",
  YourChoice: "Your cart:",
  YourSale: "Your Sale:",
  SubtotalPrice: "Subtotal Price:",
  BuyNow: "Topup Now",
  Items: "items",
  Item: "item",
  ThankYou: "Thank you",
  FinishOrderCOD: "You can use to number of order to track shipping status",
  FinishOrder:
    "Thank you so much for your purchased, to check your delivery recharge status please go to My Orders",
  NextStep: "Next Step",
  ConfirmOrder: "Confirm Order",
  RequireEnterAllFileds: "Please enter all fields",
  Error: "Error",
  InvalidEmail: "Invalid email address",
  Finish: "Finish",

  // Wishlist
  NoWishListItem: "There is no item in wishlist",
  MoveAllToCart: "Add all to cart",
  EmptyWishList: "Empty wishlist",
  EmptyAddToCart: "Sorry, the wishlist is empty",
  RemoveWishListItemConfirm: "Remove this recharge plan from wishlist?",
  CleanAll: "Clean All",

  // Sidemenu
  SignIn: "Log In",
  SignOut: "Log Out",
  GuestAccount: "Guest Account",
  CantReactEmailError:
    "We can't reach your email address, please try other login method",
  NoEmailError: "Your account don't have valid email address",
  EmailIsNotVerifiedError:
    "Your email address is not verified, we can' trust you",
  Login: "Login",
  Logout: "Logout",
  Shop: "Shop",
  Category: "Category",

  // Checkout
  Checkout: "Checkout",
  ProceedPayment: "Proceed Payment",
  Purchase: "Purchase",
  CashOnDelivery: "Cash on Delivery",
  CreditCard: "Credit Card",
  PaymentMethod: "Payment Method - Not select",
  PaymentMethodError: "Please select your payment method",
  PayWithCoD: "Your purchase will be pay when goods were delivered",
  PayWithPayPal: "Your purchase will be pay with PayPal",
  Paypal: "paypal",
  Stripe: "stripe",
  PayWithStripe: "Your purchase will be pay with Stripe",
  ApplyCoupon: "Apply",
  CouponPlaceholder: "Coupon Code",
  Apply: "Apply",
  Applying: "Applying",
  Back: "Back",
  CardNamePlaceholder: "Name written on card",
  BackToHome: "Back to Home",
  OrderCompleted: "Your order was completed",
  OrderCanceled: "Your order was canceled",
  OrderFailed: "Something went wrong...",
  OrderCompletedDesc: "Your order id is ",
  OrderCanceledDesc:
    "You have canceled the order. The transaction has not been completed",
  OrderFailedDesc:
    "We have encountered an error while processing your order. The transaction has not been completed. Please try again",
  OrderTip:
    'Tip: You could track your order status in "My Topup" section from side menu',
  Delivery: "Delivery",
  Payment: "Payment",
  Complete: "Complete",
  EnterYourFirstName: "Enter your First Name",
  EnterYourLastName: "Enter your Last Name",
  EnterYourEmail: "Enter your email",
  EnterYourPhone: "Enter your phone",
  EnterYourAddress: "Enter your address",
  CreateOrderError: "Cannot create new order. Please try again later",

  // myorder
  MyOrder: "My Topup",
  NoOrder: "You don't have any topup",
  OrderDate: "Topup Date: ",
  OrderStatus: "Status: ",
  OrderPayment: "Payment method: ",
  OrderTotal: "Total: ",
  OrderDetails: "Show detail",

  News: "News",
  PostDetails: "Post Details",
  FeatureArticles: "Feature articles",
  MostViews: "Most views",
  EditorChoice: "Editor choice",

  // settings
  Settings: "Settings",
  BASICSETTINGS: "BASIC SETTINGS",
  Language: "Language",
  INFO: "INFO",
  About: "About us",

  // language
  AvailableLanguages: "Available Languages",
  SwitchLanguage: "Switch Language",
  SwitchLanguageConfirm: "Switch language require an app reload, continue?",

  // about us
  AppName: "MHUB",
  AppDescription: "React Native template for MHub",
  AppContact: " Contact us at: mstore.io",
  AppEmail: " Email: support@mreact7.press",
  AppCopyRights: "� MHUB 2019",

  // contact us
  contactus: "Contact Us",

  // form
  NotSelected: "Not selected",
  EmptyError: "This field is empty",
  DeliveryInfo: "Delivery Info",
  FirstName: "First Name",
  LastName: "Last Name",
  Address: "Address",
  City: "Town/City",
  State: "State",
  NotSelectedError: "Please choose one",
  Postcode: "Postcode",
  Country: "Country",
  Email: "Email",
  Phone: "Phone Number",
  Note: "Note",

  // search
  Search: "Search",
  SearchPlaceHolder: "Search product by name",
  NoResultError: "Your search keyword did not match any products.",
  Details: "Details",

  // filter panel
  Categories: "Categories",
  Loading: "LOADING...",
  welcomeBack: "Welcome back! ",
  seeAll: "Show All",

  // Layout
  cardView: "Card ",
  simpleView: "List View",
  twoColumnView: "Two Column ",
  threeColumnView: "Three Column ",
  listView: "List View",
  default: "Default",
  advanceView: "Advance ",
  horizontal: "Horizontal ",

  couponCodeIsExpired: "This coupon code is expired",
  invalidCouponCode: "This coupon code is invalid",
  remove: "Remove",
  applyCouponSuccess: "Congratulations! Coupon code applied successfully ",
  reload: "Reload",

  ShippingType: "Shipping method",

  // Place holder
  TypeFirstName: "Type your first name",
  TypeLastName: "Type your last name",
  TypeAddress: "Type address",
  TypeCity: "Type your town or city",
  TypeState: "Type your state",
  TypeNotSelectedError: "Please choose one",
  TypePostcode: "Type postcode",
  TypeEmail: "Type email (Ex. acb@gmail.com), ",
  TypePhone: "Ex. (+60) 777-7777-77",
  TypeNote: "Note",
  TypeCountry: "Select country",
  SelectPayment: "Select Payment method",
  close: "CLOSE",
  noConnection: "NO INTERNET ACCESS",

  // user profile screen
  AccountInformations: "Account Informations",
  PushNotification: "Push notification",
  Privacy: "Privacy policy",
  SelectCurrency: "Select currency",
  Name: "Name",
  Currency: "Currency",
  Languages: "Languages",
  Guest: "Guest",
  FacebookLogin: "Facebook Login",
  Or: "Or",
  UserOrEmail: "Username or Email",
  DontHaveAccount: "Don't have account? ",
  accountDetails: "Account Details",
  username: "Username",
  email: "Email",
  generatePass: "Use generate password",
  password: "Password",
  signup: "Sign Up",
  profileDetail: "Profile Details",
  firstName: "First name",
  lastName: "Last name",

  // Horizontal
  featureProducts: "Feature Products",
  bagsCollections: "Bags Collections",
  womanBestSeller: "Woman Best Seller",
  manCollections: "Man Collections",

  // Modal
  Select: "Select",
  Cancel: "Cancel",

  // review
  vendorTitle: "Vendor",
  comment: "Leave a review",
  yourcomment: "Your comment",
  placeComment:
    "Tell something about your ewallet experience or leave a tip for others",
  writeReview: "Review",
  thanksForReview:
    "Thanks for the review, your content will be verify by the admin and will be published later",
  errInputComment: "Please input your content to submit",
  errRatingComment: "Please rating to submit",
  send: "Send",
  termCondition: "Term & Condition",
  Subtotal: "Subtotal",
  Discount: "Discount",
  Shipping: "Shipping",
  Recents: "Recents",
  Filters: "Filters",
  Pricing: "Pricing",
  Filter: "Filter",
  ClearFilter: "Clear Filter",
  ProductCatalog: "Product Catalog",
  ProductTags: "Product Tags",
  AddToAddress: "Add to Address"

};
